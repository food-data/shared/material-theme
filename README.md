# Material Theme

Basic HUGO Material Components Web Theme.

Contains the Frame to Transpile SCSS and Javascript resources. So the Material Web Components can be added conform the Package Documentation Conform using NPM Packages.

# Buiding Docs

- [GoHugo Raamwerk](https://gohugo.io) 

- [Material Web Componenten](https://github.com/material-components/material-components-web)

- HUGO Resource Compiler with Material Web Components as Node Modules:
    - https://discourse.gohugo.io/t/bundling-material-design-node-module-components-using-hugo-pipes-without-webpack/37978/4
    - https://discourse.gohugo.io/t/installing-dart-sass-embedded/32468/2
    - https://github.com/sass/dart-sass#from-homebrew-macos
    - https://www.brycewray.com/posts/2022/03/using-dart-sass-hugo-sequel/

Bedoeld als voorbeeld om te komen tot een gebsruikersvriendelijk Singe Page Web App die dagelijks kan worden gebruik voor het zoeken naar recepten en genereren van boodschappen lijsten.

# Setup Project

make exe: chmod 755 install.sh

## NPM Packages

Maak project aan met
- `hugo mod npm pack`
- Install Web Components
  - `npm install @material/icon-button`
  - `npm i @material/textfield`
    - etc.

## Styling

assets
- sass
- js
  - index.js


# Theme Material

Basis opzet van een Theme voor het gebruik van Material Components Web.

## assets

Opzet benodige Assets via `Node Modules`:

- js `Typescript main folder`
  - main.ts `Tavascript material etc. config instantaties`
- sass `scss styling generatie folder`
  - material.scss `material embedded dart sass styling`

## bin

Darts Sass Embedded tools die nodig zijn voor `dartsass` *transpiler*.

## layouts

### html-head.html

Laad in HTML `<head>` `Fonts`, `Icons` en `scss` styling (*transpiler* `dartsass` ToCss).

### main-js.html

Laad onderaan HTML `<body>` de Material Components Typescript Configuratie.
See https://github.com/gohugoio/hugo/releases/tag/v0.74.0
