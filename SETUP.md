# MacOs

> Requires [HomeBrew](https://brew.sh) to be installed.

Open Terminal and run the following commands

```shell
brew install go
brew install git
brew install node
brew install hugo
```

Add [NPM-config](#npm-config) to the respository.

```shell
npm run dart:sass:embedded:mac
```

# Windows

> Requires [Scoop](https://scoop.sh) to be installed.

Open Terminal and run the following commands

```shell
scoop install go
scoop install git
scoop install nodejs
scoop install hugo-extended
```

Add [NPM-config](#npm-config) to the respository.

In the open Terminal run:

```shell
npm run dart:sass:embedded:win
```

# NPM-config

Open Terminal and run the following command `hugo mod npm`

Open the created `package.json` and change the `"scripts"` configuration with next template:

```template
"scripts": {
    "dart:sass:embedded:mac": "download https://github.com/sass/dart-sass-embedded/releases/download/1.54.9/sass_embedded-1.54.9-macos-arm64.tar.gz -e -s 1 -o node_modules/.bin",
    "dart:sass:embedded:win": "download https://github.com/sass/dart-sass-embedded/releases/download/1.54.9/sass_embedded-1.54.9-windows-x64.zip -e -s 1 -o node_modules/.bin",
    "dart:sass:embedded:linux": "download https://github.com/sass/dart-sass-embedded/releases/download/1.54.9/sass_embedded-1.54.9-linux-x64.tar.gz -e -s 1 -o /usr/local/bin",
    "start": "hugo server hugo server -D -E -F --renderToDisk",
    "poststart": "git restore go.mod & rm go.sum"
}
```

> - The **Dart Sass Embedded** downloads are required to provide local interactive Styling using **Material Components for the Web**. 
> - The **HUGO Start-Script** is for local development purposes

Install the [Material Components for the Web](https://github.com/material-components/material-web) that you want to use for your App likes this:

```shell
npm install @material/<component>
```

