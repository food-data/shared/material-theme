# Partial Scripts

Use **Partial Script** to build JS en TS Script-Assets like this

```go

{{ partial "utils/partial-script" (dict 
    "partialType" .Type
    "scriptFile" "list-filter-chips.ts"
) }}

```

